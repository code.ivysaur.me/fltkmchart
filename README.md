# fltkmchart

![](https://img.shields.io/badge/written%20in-C%2B%2B%20%28FLTK%29-blue)

A 2D connected-scatter chart component for FLTK.

FLTK supplies `FL_Chart` which unfortunately doesn't support irregular x-axis spacing. This widget is loosely based on my earlier `mchart` project and should be trivial to embed into any C++ FLTK program.

Tested against FLTK 1.3.3 using Visual Studio 2013 on Windows.


## Download

- [⬇️ fltkmchart-1.0.0.zip](dist-archive/fltkmchart-1.0.0.zip) *(3.53 KiB)*
